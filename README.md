# Description

This repo is an example of 3 ways (there are more), to compile Kotlin code, it can both compile for JVM, KotlinJVM and as a natively

## Installation

Download both the native compiler and the normal compiler from the offical GitHub repo 
https://github.com/JetBrains/kotlin/releases/tag/v1.3.72

Extract both zip files and add the bin folder to your home folder

```bash
gedit .bashrc
```

Add the following line: 

```bash
export PATH=/home/YOUR_USERNAME/kotlinc/bin:$PATH
```
and

```bash
export PATH=/home/YOUR_USERNAME/kotlin-native-linux-1.3.72/bin:$PATH
```

For last but not least you have to install libinfo5, which is a dependency of Kotlin Native Compiler, for that simply open a terminal and enter the following command

```bash
sudo apt install libtinfo5
```

# IMPORTANT 
IN YOUR "kotlin-native-linux-1.3.72/bin" folder, DELETE the kotlinc script (to avoid conflicts with kotlinc from "/kotlinc/bin")

# Compilation
To compile using all optins simply type in the terminal 

```bash
make all
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
